/*
 * Copyright (C) 2013 Antonio Leiva
 * Copyright (C) 2013 Manuel Peinado
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tobolkac.demoLocal;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;




public class NavigationDrawerActivity extends Activity implements AdapterView.OnItemClickListener, LocationListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mCityNames;
    private int[] mCityImages;
    
    public static Location currentLocation;
    protected LocationManager locationManager;// = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
    protected LocationListener locationListener;
    private static String provider;
    
    private static int[] dailySpecials;
    
    private static List<Bar> Bars;
    private static List<Bar> BarsSorted;
    

    public static Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Location current) {
		currentLocation = current;
	}

	public static int[] getDailySpecials() {
		return dailySpecials;
	}

	public void setDailySpecials(int[] daily) {
		dailySpecials = daily;
	}

	public static List<Bar> getBars() {
		return Bars;
	}

	public void setBars(List<Bar> bars) {
		Bars = bars;
	}

	public static List<Bar> getBarsSorted() {
		return BarsSorted;
	}

	public static void setBarsSorted(List<Bar> barsSorted) {
		BarsSorted = barsSorted;
	}

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        setCurrentLocation(locationManager.getLastKnownLocation(provider));

        // Initialize the location fields
        
        //new GetBarInfo().execute();
        setBars(new getBarInfo().doInBackground(""));
        //setBarsSorted(getBarsSort(getBars()));
        
        
        
        
        mTitle = mDrawerTitle = getTitle();
        mCityNames = getResources().getStringArray(R.array.drawer_items);
        TypedArray typedArray = getResources().obtainTypedArray(R.array.city_images);
        mCityImages = new int [typedArray.length()];
        for (int i = 0; i < typedArray.length(); ++i) {  
            mCityImages[i] = typedArray.getResourceId(i, 0);
        }
        typedArray.recycle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mCityNames));
        mDrawerList.setOnItemClickListener(this);

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectItem(0);
        }
        
    }

 
    

	@Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return mDrawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectItem(position);
    }

    private void selectItem(int position){
        // update the main content by replacing fragments
    	if (mCityNames[position].equals("Drank")){
    		
	        Fragment fragment = new MyListFragment();
	        Bundle args = new Bundle();
	        args.putInt(MyListFragment.ARG_IMAGE_RES, mCityImages[position]);
	        args.putInt(MyListFragment.ARG_ACTION_BG_RES, R.drawable.ab_background);
	        fragment.setArguments(args);
	        
	        //sun through sat
	        int[] speD = {11, 32, 17, 25, 10, 12, 42};
	        setDailySpecials(speD);
	        currentLocation = getCurrentLocation();
	        //Collections.sort(Bars, new BarLocationComparator());
	        
	        //new GetBarInfo().execute();
	
	        FragmentManager fragmentManager = getFragmentManager();
	        
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
	        
    	}
    	else if (mCityNames[position].equals("Localeur")){
    		
	        Fragment fragment = new LocaleurFragment();
	        
	        
	
	        FragmentManager fragmentManager = getFragmentManager();
	        
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
	        
    	}
    	else if (mCityNames[position].equals("Sports")){
    		
	        Fragment fragment = new SampleFragment();
	        Bundle args = new Bundle();
	        args.putInt(MyListFragment.ARG_IMAGE_RES, mCityImages[position]);
	        args.putInt(MyListFragment.ARG_ACTION_BG_RES, R.drawable.ab_background);
	        fragment.setArguments(args);
	
	        FragmentManager fragmentManager = getFragmentManager();
	        
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
	        
    	}
    	else{
    		Fragment fragment = new SampleFragment();
	        Bundle args = new Bundle();
	        args.putInt(SampleFragment.ARG_IMAGE_RES, mCityImages[position]);
	        args.putInt(SampleFragment.ARG_ACTION_BG_RES, R.drawable.ab_background);
	        fragment.setArguments(args);
	
	        FragmentManager fragmentManager = getFragmentManager();
	        
	        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
	        
	        setTitle(mCityNames[position]);
    	}

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        if(mCityNames[position].equals("Drank")){setTitleDay();}
        else{setTitle(mCityNames[position]);}
        mDrawerLayout.closeDrawer(mDrawerList);
    }
    private void setTitleDay() {
		Date d = new Date();
		SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
		mTitle = simpleDateformat.format(d) + " Drink Specials";
		getActionBar().setTitle(mTitle);
		
	}

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
    public class BarLocationComparator implements Comparator<Bar> {
    	
		@Override
		public int compare(Bar b1, Bar b2) {
			Location bar1Loc = new Location("bar1");
			bar1Loc.setLatitude(Double.parseDouble(b1.getLat()));
			bar1Loc.setLatitude(Double.parseDouble(b1.getLng()));
			Location bar2Loc = new Location("bar2");
			bar2Loc.setLatitude(Double.parseDouble(b2.getLat()));
			bar2Loc.setLatitude(Double.parseDouble(b2.getLng()));
			if (currentLocation.distanceTo(bar1Loc) < currentLocation.distanceTo(bar2Loc))
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
    }
    public LatLng getLocation(){
		LocationManager locMgr = ((LocationManager) getSystemService(LOCATION_SERVICE));
		Location location = locMgr.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
		int lat = ((int) location.getLatitude());
		int lon = ((int) location.getLongitude());
		LatLng loc = new LatLng(lat, lon);
		return loc;
	}
    
    public class getBarInfo extends AsyncTask<String, Integer, List<Bar>>{
    	List<Bar> a = new ArrayList<Bar>();
    		protected void onPreExecute(){
    		        //Setup is done here
    		    }
    		    @Override
    		    protected List<Bar> doInBackground(String... params) {
    		        //TODO Auto-generated method stub
    		    	try {
    		    		
    					String url = "http://www.drankbank.com/scripts/parse-to-xml-bars.php?city_level=normal&db_city=Austin";
    					DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    					DocumentBuilder db = dbf.newDocumentBuilder();
    					InputStream s = getAssets().open("barInfo.xml");
    					
    					String b = "";
    					
    					BufferedReader reader = new BufferedReader(new InputStreamReader(s));
    					String line;
    					while ((line = reader.readLine()) != null)
    					{
    						b += line;
    					}
    					String[] bars = b.split("<marker ");
    				      String[] barsClean = new String[bars.length - 1];
    				      for(int c = 1; c<bars.length;c++){
    				    	  String b2 = bars[c].replace("/>", "");
    				    	  b2 = b2.replace("&#39;", "'");
    				    	  b2 = b2.replace("&eacute;", "�");
    				    	  barsClean[c-1] = b2;
    				      }
    					
    				    for (String bar : barsClean)
    				    {
    				    	Bar curr = new Bar();
    				    	curr.setName(getBarAttr(bar, "name"));
    				    	curr.setPhone(getBarAttr(bar, "phone"));
    				    	curr.setAddress(getBarAttr(bar, "address"));
    				    	curr.setCity(getBarAttr(bar, "city"));
    				    	curr.setLat(getBarAttr(bar, "lat"));
    				    	curr.setLng(getBarAttr(bar, "lng"));
    				    	String[] days = {"mon", "tues", "weds", "thur", "fri", "sat", "sun"};
    				    	for (String day : days)
    				    	{
    					    	List<Special> specials = new ArrayList<Special>();
    					    	for(int m = 0; m<3;m++)
    					    	{				    
    					    		Special sp = new Special();
    					    		sp.setTime(getBarAttr(bar, day+"_t"+(m+1)));
    					    		String[] deals = new String[3];
    					    		for(int d = 0; d<3;d++)
    						    	{
    					    			deals[d] = getBarAttr(bar, day+"_t"+(m+1) + "_d"+(d+1));
    						    	}
    					    		sp.setDeal(deals);
    					    		specials.add(sp);
    					    	}
    					    	if(day.equals("mon")){curr.setMonSpecials(specials);}
    					    	if(day.equals("tues")){curr.setTueSpecials(specials);}
    					    	if(day.equals("weds")){curr.setWedSpecials(specials);}
    					    	if(day.equals("thur")){curr.setThurSpecials(specials);}
    					    	if(day.equals("fri")){curr.setFriSpecials(specials);}
    					    	if(day.equals("sat")){curr.setSatSpecials(specials);}
    					    	if(day.equals("sun")){curr.setSunSpecials(specials);}
    					    	
    				    	}
    				    	a.add(curr);
    				    }
    					
    					
    					
    				}catch (Exception e) {
    					System.out.println("XML Pasing Excpetion = " + e);
    				}
    		    	return a;
    		  	}
    		    @Override
    		    protected void onPostExecute(List<Bar> result) {
    		    	// TODO Auto-generated method stub
    		    	super.onPostExecute(result);
    		    }
    		    protected void onProgressUpdate(Integer... params){
    		        //Update a progress bar here, or ignore it, it's up to you
    		    }
    		    
    		        protected void onCancelled(){
    		        }
    		    }
    public String[] getBarNames(List<Bar> a3){
  	  String[] names = new String[a3.size()];
  	  int c = 0;
  	  for (Bar b : a3){
  		  names[c++]=b.getName();
  	  }
  	  return names;
    }
    
    private String getBarAttr(String string, String string2) {
  	// TODO Auto-generated method stub
  	String out = string.substring(string.indexOf(string2) + (string2.length()+2));
  	out = out.substring(0, out.indexOf("\""));
  	return out;
  }

	
	

	
	@Override
	  public void onLocationChanged(Location location) {
	    
	  }

	  @Override
	  public void onStatusChanged(String provider, int status, Bundle extras) {
	    // TODO Auto-generated method stub

	  }

	  @Override
	  public void onProviderEnabled(String provider) {
	    Toast.makeText(this, "Enabled new provider " + provider,
	        Toast.LENGTH_SHORT).show();

	  }

	  @Override
	  public void onProviderDisabled(String provider) {
	    Toast.makeText(this, "Disabled provider " + provider,
	        Toast.LENGTH_SHORT).show();
	  }
	  
	  public class MyPagerAdapter extends android.support.v4.app.FragmentPagerAdapter {

			private final String[] TITLES = { "Categories", "Home", "Top Paid", "Top Free", "Top Grossing", "Top New Paid",
					"Top New Free", "Trending" };

			public MyPagerAdapter(android.support.v4.app.FragmentManager fm) {
				super(fm);
			}

			@Override
			public CharSequence getPageTitle(int position) {
				return TITLES[position];
			}

			@Override
			public int getCount() {
				return TITLES.length;
			}

			@Override
			public android.support.v4.app.Fragment getItem(int position) {
				return SuperAwesomeCardFragment.newInstance(position);
			}
	  }
}
