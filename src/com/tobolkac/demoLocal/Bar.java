package com.tobolkac.demoLocal;

import java.util.List;

public class Bar {
	private String name;
	private String address;
	private String phone;
	private String city;
	private String lng;
	
	private String lat;
	
	private List<Special> monSpecials;
	private List<Special> tueSpecials;
	private List<Special> wedSpecials;
	private List<Special> thurSpecials;
	private List<Special> friSpecials;
	private List<Special> satSpecials;
	private List<Special> sunSpecials;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public List<Special> getMonSpecials() {
		return monSpecials;
	}
	public void setMonSpecials(List<Special> monSpecials) {
		this.monSpecials = monSpecials;
	}
	public List<Special> getTueSpecials() {
		return tueSpecials;
	}
	public void setTueSpecials(List<Special> tueSpecials) {
		this.tueSpecials = tueSpecials;
	}
	public List<Special> getWedSpecials() {
		return wedSpecials;
	}
	public void setWedSpecials(List<Special> wedSpecials) {
		this.wedSpecials = wedSpecials;
	}
	public List<Special> getThurSpecials() {
		return thurSpecials;
	}
	public void setThurSpecials(List<Special> thurSpecials) {
		this.thurSpecials = thurSpecials;
	}
	public List<Special> getFriSpecials() {
		return friSpecials;
	}
	public void setFriSpecials(List<Special> friSpecials) {
		this.friSpecials = friSpecials;
	}
	public List<Special> getSatSpecials() {
		return satSpecials;
	}
	public void setSatSpecials(List<Special> satSpecials) {
		this.satSpecials = satSpecials;
	}
	public List<Special> getSunSpecials() {
		return sunSpecials;
	}
	public void setSunSpecials(List<Special> sunSpecials) {
		this.sunSpecials = sunSpecials;
	}
	
}
