package com.tobolkac.demoLocal;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.AsyncTask;

class GetBarInfo extends AsyncTask<Void, Void, List<Bar>> {
	ProgressDialog progressDialog;
        @Override
        protected void onPreExecute() {
           // Before starting background task
           // Show Progress Dialog etc,.
        }
 
        protected List<Bar> doInBackground(Void... unused) {
           
                // Run actual background task
                // Like sending HTTP Request - Parsing data
			String url = "http://www.drankbank.com/scripts/parse-to-xml-bars.php?city_level=normal&db_city=Austin";

            XMLParser parser = new XMLParser();
            String xml = parser.getXmlFromUrl(url);
            Document doc = parser.getDomElement(xml);
            NodeList elms = doc.getElementsByTagName("marker");
            
            List<Bar> Bars = new ArrayList<Bar>();
            
            for (int count = 0; count<elms.getLength(); count++)
		    {
            	Element b = (Element) elms.item(count);
		    	Bar curr = new Bar();
		    	curr.setName(b.getAttribute("name"));
		    	curr.setPhone(b.getAttribute("phone"));
		    	curr.setAddress(b.getAttribute("address"));
		    	curr.setCity(b.getAttribute("city"));
		    	curr.setLat(b.getAttribute("lat"));
		    	curr.setLng(b.getAttribute("lng"));
		    	String[] days = {"mon", "tues", "weds", "thur", "fri", "sat", "sun"};
		    	for (String day : days)
		    	{
			    	List<Special> specials = new ArrayList<Special>();
			    	for(int m = 0; m<3;m++)
			    	{				    
			    		Special sp = new Special();
			    		sp.setTime(b.getAttribute(day+"_t"+(m+1)));
			    		String[] deals = new String[3];
			    		for(int d = 0; d<3;d++)
				    	{
			    			deals[d] = b.getAttribute(day+"_t"+(m+1) + "_d"+(d+1));
				    	}
			    		sp.setDeal(deals);
			    		specials.add(sp);
			    	}
			    	if(day.equals("mon")){curr.setMonSpecials(specials);}
			    	if(day.equals("tues")){curr.setTueSpecials(specials);}
			    	if(day.equals("weds")){curr.setWedSpecials(specials);}
			    	if(day.equals("thur")){curr.setThurSpecials(specials);}
			    	if(day.equals("fri")){curr.setFriSpecials(specials);}
			    	if(day.equals("sat")){curr.setSatSpecials(specials);}
			    	if(day.equals("sun")){curr.setSunSpecials(specials);}
			    	
		    	}
		    	Bars.add(curr);
		    }
            return (null);
        }
 
        protected void onPostExecute(List<Bar> result) {
            // On completing background task
            // closing progress dialog etc,.
        	NavigationDrawerActivity.setBarsSorted(result);
        }
}