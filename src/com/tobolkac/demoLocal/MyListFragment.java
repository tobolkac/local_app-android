package com.tobolkac.demoLocal;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.manuelpeinado.fadingactionbar.FadingActionBarHelper;


public class MyListFragment extends ListFragment {
List<String> barNames = new ArrayList<String>();
public String a2 ="";


private static List<Bar> BarsList;

public Location currentLocation;

String[] values = new String[47];

public static List<Bar> getBarsList() {
	return BarsList;
}

public static void setBarsList(List<Bar> barsList) {
	BarsList = barsList;
}

@Override
  public void onActivityCreated(Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
   
  }
  
  private FadingActionBarHelper mFadingHelper;
  private Bundle mArguments;

  public static final String ARG_IMAGE_RES = "image_source";
  public static final String ARG_ACTION_BG_RES = "image_action_bs_res";
  
  public class MyListAdapter extends ArrayAdapter<String> {
	  
	  Context myContext;
	  List<Bar> Bars;
	  
	  public MyListAdapter(Context context, int textViewResourceId,
	    String[] values) {
		  
	   super(context, textViewResourceId, values);
	   this.Bars = NavigationDrawerActivity.getBars();
	   myContext = context;
	  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
		   //return super.getView(position, convertView, parent);
			  View row;
			  if(position == values.length-1)
			  {
				  LayoutInflater inflater = 
						     (LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				  row=inflater.inflate(R.layout.load_more_button, parent, false);
			  }
			  else{
				  Bar bar = getBarsList().get(position);
				  
				  LayoutInflater inflater = 
				     (LayoutInflater)myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				   row=inflater.inflate(R.layout.my_list_row, parent, false);
				   TextView firstLine =(TextView)row.findViewById(R.id.barName);
				   firstLine.setText(bar.getName());
				   
				   List<Special> specials = getDaySpecials(bar);
				   
				   
				   
				   Special s = specials.get(0);
				   TextView time1 = (TextView) row.findViewById(R.id.dealTime1);
				   
				   String deals = s.getTime();
				   String[] sp = s.getDeal(); 
		        	  for(String d : sp)
		        	  {
		        		  if (!d.equals("")){
		        			  deals += "\n\t- " + d;
		        		  }
		        	  }
	        	  
	        	  time1.setText(deals);
			   
				   
				   s = specials.get(1);
				   TextView time2 = (TextView) row.findViewById(R.id.dealTime2);
				   deals = "";
				   if (!s.getTime().equals("")){
				   deals = s.getTime();
				   sp = s.getDeal(); 
		        	  for(String d : sp)
		        	  {
		        		  if (!d.equals("")){
		        			  deals += "\n\t- " + d;
		        		  }
		        	  }
				   }
	        	  time2.setText(deals);
			  }
				 
			  
		   
		   
		   return row;
		  }

		

		 }
  
  @Override
  public void onCreate(Bundle savedInstanceState) {
   super.onCreate(savedInstanceState);
   
   /*
   ListAdapter myListAdapter = new ArrayAdapter<String>(
     getActivity(),
     android.R.layout.simple_list_item_1,
     month);
   setListAdapter(myListAdapter);
   */
   
   currentLocation = NavigationDrawerActivity.getCurrentLocation();
   
   setBarsList(removeNoSpecials(NavigationDrawerActivity.getBars()));
   //Collections.sort(BarsList, new BarLocationComparator());
   //BarsList = sortBars(BarsList);
   values = new String[getBarsList().size()+1];
  
   List<Bar> Bars = null;
   MyListAdapter myListAdapter = 
     new MyListAdapter(getActivity(), R.layout.my_list_row, values);
   setListAdapter(myListAdapter);
  }

  
  private List<Bar> removeNoSpecials(List<Bar> bars2) {
		List<Bar> retBars = new ArrayList<Bar>();
		for (Bar b :bars2){
			if (!getDaySpecials(b).get(0).getTime().equals(""))
			{
				retBars.add(b);
			}
		}
		return retBars;
	}
  
  public List<Bar> sortBars(List<Bar> bars)
  {
	  Collections.sort(bars, new BarLocationComparator());
	  return bars;
  }
  public class BarLocationComparator implements Comparator<Bar> {
  	
		@Override
		public int compare(Bar b1, Bar b2) {
			Location bar1Loc = new Location("bar1");
			bar1Loc.setLatitude(Double.parseDouble(b1.getLat()));
			bar1Loc.setLongitude(Double.parseDouble(b1.getLng()));
			Location bar2Loc = new Location("bar2");
			bar2Loc.setLatitude(Double.parseDouble(b2.getLat()));
			bar2Loc.setLongitude(Double.parseDouble(b2.getLng()));
			if (currentLocation.distanceTo(bar1Loc) < currentLocation.distanceTo(bar2Loc))
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
  }
  private List<Special> getDaySpecials(Bar bar) {
		Calendar calendar = Calendar.getInstance();
		int d = calendar.get(Calendar.DAY_OF_WEEK);
		List<Special> s = new ArrayList<Special>();
		switch(d){
		   case 1:
			   s = bar.getSunSpecials();
			   break;
		   case 2:
			   s = bar.getMonSpecials();
			   break;
		   case 3:
			   s = bar.getTueSpecials();
			   break;
		   case 4:
			   s = bar.getWedSpecials();
			   break;
		   case 5:
			   s = bar.getThurSpecials();
			   break;
		   case 6:
			   s = bar.getFriSpecials();
			   break;
		   case 7:
			   s = bar.getSatSpecials();
			   break;
		   }
		return s;
	}
  
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      View view = mFadingHelper.createView(inflater);

      if (mArguments != null){
    	  
    	  Calendar calendar = Calendar.getInstance();
	 	  int day = calendar.get(Calendar.DAY_OF_WEEK);
	 	  //sun to sat
	 	  int[] specialPic = {R.drawable.chain_drive_austin_tx, R.drawable.parkside_austin_tx, R.drawable.crown_and_anchor_pub_austin_tx,  R.drawable.kung_fu_saloon_austin_tx,  R.drawable.cedar_door_bar_and_grill_austin_tx, 
	 			  R.drawable.champions_austin_tx,  R.drawable.the_side_bar_austin_tx};
		      
		   ImageView barImg = (ImageView) view.findViewById(R.id.image_bar_special);
	       barImg.setImageResource(specialPic[day-1]);
	       TextView barName = (TextView) view.findViewById(R.id.textView_name_special);
	       Bar bar = NavigationDrawerActivity.getBars().get(NavigationDrawerActivity.getDailySpecials()[day-1]);
	       barName.setText(bar.getName());
	       List<Special> specials = bar.getMonSpecials();
	       String deals = "";
	       Special s = specials.get(0);
	     	  deals += s.getTime();
	     	  String[] sp = s.getDeal(); 
	     	  for(String d : sp)
	     	  {
	     		  if (!d.equals("")){
	     			  deals += "\n\t- " + d;
	     		  }
	     	  }
	       
	       TextView dealsText = (TextView) view.findViewById(R.id.textView_deal_special);
	       dealsText.setText(deals);

	      
          
      }
      
      //values = getBarNames(TestActivity.bars);
      //ListView lv = (ListView) view.findViewById(R.id.list);
  	  
      
      return view; //inflater.inflate(R.layout.fragment_item_list, container, false);
  }

  @Override
  public void onAttach(Activity activity) {
      super.onAttach(activity);

      mArguments = getArguments();
      int actionBarBg = mArguments != null ? mArguments.getInt(ARG_ACTION_BG_RES) : R.drawable.ab_background_light;
      
      
      mFadingHelper = new FadingActionBarHelper()
          .actionBarBackground(actionBarBg)
          .headerLayout(R.layout.header)
          .contentLayout(R.layout.fragment_item_list)
          .lightActionBar(actionBarBg == R.drawable.ab_background_light);
      mFadingHelper.initActionBar(activity);
   
  }

 @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
	 
    Intent intent = new Intent(getActivity(), Bar_Info.class);
    intent.putExtra("Bar", position-1);
    
    startActivity(intent);
	  

  }
  
 
} 
