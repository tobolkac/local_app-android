package com.tobolkac.demoLocal;



import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.support.v4.app.NavUtils;

public class Bar_Info extends Activity {
	String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thuresday", "Friday", "Saturday", "Sunday"};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bar__info);
		// Show the Up button in the action bar.
		setupActionBar();
		TextView barName = (TextView) findViewById(R.id.barInfoName);
		TextView barAddress = (TextView) findViewById(R.id.barInfoAddress);
		TextView barPhone = (TextView) findViewById(R.id.barInfoPhone);
		
		Bundle extras = getIntent().getExtras();
		if (extras == null) {
		    return;
		    }
		// Get data via the key
		int value1 = extras.getInt("Bar");
		final Bar bar = MyListFragment.getBarsList().get(value1);
		if (value1 >= 0) {
			barName.setText(bar.getName());
			barAddress.setText(bar.getAddress() + " " + bar.getCity());
			barPhone.setText(bar.getPhone());
		}
		barAddress.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String uri = "geo:"+ bar.getLat() + "," + bar.getLng();
				startActivity(new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri)));
				
			}
		});
		
//		GoogleMap mMap = ((MapView) findViewById(R.id.barMap)).getMap();
//		
//		LatLng loc = new LatLng(Double.parseDouble(bar.getLat()), Double.parseDouble(bar.getLng()));
//		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 15));
		
		com.google.android.gms.maps.MapFragment map = (com.google.android.gms.maps.MapFragment) getFragmentManager().findFragmentById(R.id.barMap);
		GoogleMap gMap = map.getMap();
		LatLng barLocation = new LatLng(Double.parseDouble(bar.getLat()), Double.parseDouble(bar.getLng()));
		gMap.setMyLocationEnabled(true);
		//Location myLocation = gMap.getMyLocation();
		gMap.addMarker(new MarkerOptions().position(barLocation));
		//LatLng myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
		gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(barLocation, 15));
		

		TextView locTest = (TextView) findViewById(R.id.locTest);
		
	}
	public LatLng getLocation(){
		LocationManager locMgr = ((LocationManager) getSystemService(LOCATION_SERVICE));
		Location location = locMgr.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
		int lat = ((int) location.getLatitude());
		int lon = ((int) location.getLongitude());
		LatLng loc = new LatLng(lat, lon);
		return loc;
	}
	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.bar__info, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
